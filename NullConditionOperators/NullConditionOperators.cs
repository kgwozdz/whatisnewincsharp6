﻿using System;
using System.Collections.Generic;

namespace NullConditionOperators
{
    public class NullConditionOperators
    {
        static void Main(string[] args)
        {
            Car newCar = new Car
            { 
                Name = "Peugeot 206",
                Owners = new List<Person>
                {
                    new Person { Name = "Tomek" },
                    new Person { Name = "Zosia" },
                    new Person { Name = null }
                }
            };

            OldDisplayInformation(newCar);
            NewDisplayInformation(newCar);

            newCar.Owners = null;

            OldDisplayInformation(newCar);
            NewDisplayInformation(newCar);

            OldDisplayInformation(null);
            NewDisplayInformation(null);

            Console.ReadLine();
        }

        public static void OldDisplayInformation(Car car)
        {
            // Before C# 6.0
            if (car == null)
                return;
            Console.WriteLine(car.Name);

            if (car.Owners != null && car.Owners[0] != null)
            {                
                Console.WriteLine(car.Owners[0].Name != null ? car.Owners[0].Name.ToUpper() : "");
            }
        }

        public static void NewDisplayInformation(Car car)
        {
            // In C# 6.0
            Console.WriteLine(car?.Name);
            Console.WriteLine(car?.Owners?[0]?.Name?.ToUpper());
        }
    }

    public class Car
    {
        public string Name;
        public List<Person> Owners;
    }

    public class Person
    {
        public string Name;
    }
}
