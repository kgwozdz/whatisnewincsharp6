﻿using System;

namespace InitializersForAutoProperties
{
    class InitializersForAutoProperties
    {
        static void Main(string[] args)
        {
            Console.WriteLine(new Person().Name);
            Console.ReadLine();
        }
    }

    class Person
    {
        // In C# 6.0
        public string Name { get; set; } = "Amadeusz";

        public Person()
        {
            // Before C# 6.0
            Name = "Tomek";
        }
    }
}
