﻿using System;

namespace StringInterpolation
{
    class StringInterpolation
    {
        static void Main(string[] args)
        {
            Person TheKing = new Person { FirstName = "Elvis", LastName = "Presley"};

            // Before C# 6.0
            Console.WriteLine("" + TheKing.FirstName + " " + TheKing.LastName + ".");
            Console.WriteLine(string.Format("{0} {1}.", TheKing.FirstName, TheKing.LastName));

            // In C# 6.0
            Console.WriteLine($"{TheKing.FirstName} \n {TheKing.LastName}.");
            
            Console.ReadLine();
        }
    }

    class Person
    {
        public string FirstName;
        public string LastName;
    }
}
