﻿using System;

namespace ExceptionFilters
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 10, b = 0;

            // Before C# 6.0
            try
            {
                int c = a / b;
            }
            catch (DivideByZeroException e)
            {
                Log(e);
                if (e.InnerException == null && a != 0)
                {
                    //...
                }
                else
                {
                    //...
                }
            }
            // In C# 6.0
            try
            {
                int c = a / b;
            }
            catch (DivideByZeroException e) when (a != 0 & Log(e))
            {
                //...
            }
            catch (DivideByZeroException e)
            {
                //...
            }
        }

        static bool Log(Exception e)
        {
            //Log Exception
            return true; ;
        }
    }
}
