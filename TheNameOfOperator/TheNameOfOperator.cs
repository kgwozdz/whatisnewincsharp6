﻿using System;

namespace TheNameOfOperator
{
    class TheNameOfOperator
    {
        static void Main(string[] args)
        {
            OldExceptioner(null);
            NewExceptioner(null);
        }

        private static void OldExceptioner(string variable)
        {
            // Before C# 6.0
            try
            {
                Console.WriteLine(variable.Length);
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine("variable is null");
                Console.ReadLine();
            }
        }

        private static void NewExceptioner(string variable)
        {
            // In C# 6.0
            try
            {
                Console.WriteLine(variable.Length);
            }
            catch (NullReferenceException)
            {
                Console.WriteLine(nameof(variable) + " is null");
                Console.ReadLine();
            }
        }
    }
}
