﻿namespace UsingStaticClasses
{
    // Before C# 6.0
    using System;

    class OldProgram
    {
        public static void OldMain()
        {
            double squareOf13 = Math.Sqrt(13);
            Console.WriteLine(squareOf13);
        }
    }
}

namespace UsingStaticClasses
{
    // In C# 6.0
    using static System.Math;
    using static System.Console;

    class NewProgram
    {
        public static void NewMain()
        {
            double squareOf13 = Sqrt(13);
            WriteLine(squareOf13);
        }
    }
}

namespace UsingStaticClasses
{
    using static System.Linq.ParallelEnumerable;
    class UsingStatic
    {
        static void Main(string[] args)
        {
            OldProgram.OldMain();
            NewProgram.NewMain();

            string[] arrayOfNames = new string[] { "Elvis", "Thomas", "Wojtek"};
            arrayOfNames.AsParallel().ForAll(a => System.Console.WriteLine(a));
            
            System.Console.ReadLine();
        }
    }
}
