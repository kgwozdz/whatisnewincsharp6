﻿using System.Threading.Tasks;
using System;
using System.Net.Http;

namespace AwaitInCatchAndFinallyBlock
{
    class AwaitInCatchAndFinallyBlock
    {
        private static void Main(string[] args)
        {
            Task.Factory.StartNew(() => GetWeatherAsync());
            Console.ReadKey();
        }

        private async static Task GetWeatherAsync()
        {
            HttpClient client = new HttpClient();
            try
            {
                var result = await client.GetStringAsync("http://jakitydzien/");
                Console.WriteLine(result + "\n\nWszystko ok.");
            }
            catch (Exception exception)
            {
                try
                {
                    var result = await client.GetStringAsync("http://jakitydzien.pl/");
                    Console.WriteLine(result + "\n\nCoś poszło nie tak");
                }
                catch (Exception)
                {
                    //...
                }
            }
        }
    }
}