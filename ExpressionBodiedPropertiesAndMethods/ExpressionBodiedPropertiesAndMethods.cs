﻿using System;

namespace ExpressionBodiedPropertiesAndMethods
{
    class ExpressionBodiedPropertiesAndMethods
    {
        static void Main(string[] args)
        {
            Person person = new Person();

            Console.WriteLine(person.OldGetInfo());
            Console.WriteLine("Date Of Birth: " + person.OldDateOfBirth);
            Console.WriteLine(person.NewGetInfo());
            Console.WriteLine("Date Of Birth: " + person.NewDateOfBirth);

            Console.ReadLine();
        }
    }

    class Person
    {
        public string Name = "Daniel";
        public int Age = 39;

        // Before C# 6.0
        public int OldDateOfBirth
        {
            get
            {
                return DateTime.Now.Year - Age;
            }
            set { }
        }

        public string OldGetInfo()
        {
            return Name + " is years " + Age + " old.";
        }

        // In C# 6.0
        public int NewDateOfBirth => DateTime.Now.Year - Age;
        public string NewGetInfo() => Name + " is years " + Age + " old.";
    }
}
