﻿using System;
using System.Collections.Generic;

namespace IndexInitializers
{
    class IndexInitializers
    {
        static void Main(string[] args)
        {
            // Before C# 6.0
            Dictionary<int, string> OldTypesOfUsers = new Dictionary<int, string>()
            {
                { 1, "Administrator"},
                { 2, "Moderator"},
                { 3, "Logged User"}
            };
            OldTypesOfUsers[4] = "Guest";

            foreach (var type in OldTypesOfUsers)
            {
                Console.WriteLine("Key:" + type.Key + " Value:" + type.Value);
            }


            // In C# 6.0
            Dictionary<int, string> NewTypesOfUsers = new Dictionary<int, string>()
            {
                [1] = "Administrator",
                [2] = "Moderator",
                [3] = "Logged User"
            };
            NewTypesOfUsers[4] = "Guest";

            foreach (var type in NewTypesOfUsers)
            {
                Console.WriteLine("Key:" + type.Key + " Value:" + type.Value);
            }

            Console.ReadLine();
        }
    }
}
