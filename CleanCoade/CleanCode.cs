﻿namespace CleanCode
{
    //Before c# 6.0
    using System;
    class OldProgram
    {
        public OldProgram()
        {
            X = 10;
            Y = 5;
        }

        public int X { get; private set; }
        public int Y { get; private set; }
        public double Distance
        {
            get { return Math.Sqrt(X * X + Y * Y); }
            private set { }
        }

        public override string ToString()
        {
            return string.Format("x: {0} y: {1}", X, Y);
        }
    }
}

namespace CleanCode
{
    //In c# 6.0
    using static System.Math;
    class NewProgram
    {
        public int X { get; } = 10;
        public int Y { get; } = 5;
        public double Distance => Sqrt(X * X + Y * Y);

        public override string ToString() => $"x: {X} y: {Y}";
    }
}
